package com.example.contactmanager;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withChild;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class UITest {

    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void isContactListFragmentComplete() {
        onView(withId(R.id.rvContacts)).check(matches(isDisplayed()));
        onView(withId(R.id.fab)).check(matches(isDisplayed()));
    }

    @Test
    public void testLoadAddContactFragment() {
//        Perform an FAB click event
        onView(withId(R.id.fab)).perform(click());

//        Check whether AddContactFragment showing
        onView(withId(R.id.fragmentAddContact)).check(matches(isDisplayed()));

//        Check whether FullName EditText showing
        onView(withId(R.id.txtFullName)).check(matches(isDisplayed()));

//        Check whether ContactNumber EditText showing
        onView(withId(R.id.txtNumber)).check(matches(isDisplayed()));
    }

    @Test
    public void testAddContact() {
        int sufix = new Random().nextInt(800 - 100) + 100;
        String dummyName = "Test User " + sufix;
        String dummyNumber = "0774821" + sufix;

//        Perform an FAB click event
        onView(withId(R.id.fab)).perform(click());

//        Type User name & Contact NUmber
        onView(withId(R.id.txtFullName)).perform(typeText(dummyName));
        onView(withId(R.id.txtNumber)).perform(typeText(dummyNumber), closeSoftKeyboard());

//        Press Done button and Add Contact
        onView(withId(R.id.fab)).perform(click());

//        Check whether added contact appear on recycler view
//        onView(withText(dummyName)).check(matches(isDisplayed()));
        onView(allOf(withChild(withText(dummyName)), withChild(withText(dummyNumber)))).check(matches(isDisplayed()));
    }
}