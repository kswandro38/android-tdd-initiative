package com.example.contactmanager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.contactmanager.model.Contact;

import java.util.HashMap;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {

    private HashMap<Integer, Contact> contactMap;
    private static final String TAG = "ContactAdapter";

    public ContactAdapter(HashMap<Integer, Contact> contactMap) {
        this.contactMap = contactMap;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
        return new ContactAdapter.ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        Contact contact = contactMap.get(position);
        holder.txtContactName.setText(contact.getName());
        holder.txtContactNumber.setText(contact.getNumber());
        holder.id = contact.getId();
        Log.d(TAG, "onBindViewHolder: " + contact.toString());
    }

    @Override
    public int getItemCount() {
        return contactMap.size();
    }

    class ContactViewHolder extends RecyclerView.ViewHolder {

        int id;
        TextView txtContactName;
        TextView txtContactNumber;

        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);

            txtContactName = itemView.findViewById(R.id.txtContactName);
            txtContactNumber = itemView.findViewById(R.id.txtContactNumber);
        }
    }
}
