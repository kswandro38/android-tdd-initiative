package com.example.contactmanager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.contactmanager.model.Contact;
import com.example.contactmanager.model.dummy.DummyContacts;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.HashMap;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ContactListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactListFragment extends Fragment {

    private MainActivity activity;
    private RecyclerView rvContacts;
    private ContactAdapter contactAdapter;
    private HashMap<Integer, Contact> contactMap;
    private static final String TAG = "ContactListFragment";

    public ContactListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ContactListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ContactListFragment newInstance() {
        ContactListFragment fragment = new ContactListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = ((MainActivity) getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View fragmentView = inflater.inflate(R.layout.fragment_contact_list, container, false);
        rvContacts = fragmentView.findViewById(R.id.rvContacts);

        setupRecyclerView();
        setupToolBar();
        setupFAB();
//        loadDummyData();

        return fragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    private void setupRecyclerView() {
        contactMap = new HashMap<>();
        contactAdapter = new ContactAdapter(contactMap);

        rvContacts.setLayoutManager(new LinearLayoutManager(getContext()));
        rvContacts.setAdapter(contactAdapter);
    }

    private void setupToolBar() {
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
        Objects.requireNonNull(((MainActivity) getActivity()).getSupportActionBar()).setDisplayShowHomeEnabled(false);
    }

    private void setupFAB() {
        FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        fab.setImageDrawable(getActivity().getDrawable(R.drawable.ic_add));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(ContactListFragment.this).navigate(R.id.action_ContactListFragment_to_AddContactFragment);
            }
        });
    }

    private void loadData() {
        contactMap.putAll(activity.getContactList());
        contactAdapter.notifyDataSetChanged();
    }

    private void loadDummyData() {
        contactMap.putAll(DummyContacts.ITEM_MAP);
        contactAdapter.notifyDataSetChanged();
    }
}