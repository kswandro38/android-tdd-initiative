package com.example.contactmanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.Navigation;

import com.example.contactmanager.model.Contact;
import com.google.gson.Gson;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private HashMap<Integer, Contact> contactHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPreferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            Navigation.findNavController(this, R.id.nav_host_fragment).navigate(R.id.action_AddContactFragment_to_ContactListFragment);
        }
        return super.onOptionsItemSelected(item);
    }

    public HashMap<Integer, Contact> getContactList() {
        // load contacts from preference
        String contactListString = sharedPreferences.getString(getString(R.string.contact_list), null);
        return contactListString!=null ? new Gson().fromJson(contactListString, ContactList.class).getMap()
                : new HashMap<Integer, Contact>();
    }

    public void setContactList(HashMap<Integer, Contact> contactHashMap){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getString(R.string.contact_list), new Gson().toJson(new ContactList(contactHashMap)));
        editor.apply();
    }

    class ContactList{
        HashMap<Integer, Contact> map;

        public ContactList(HashMap<Integer, Contact> map) {
            this.map = map;
        }

        public HashMap<Integer, Contact> getMap() {
            return map;
        }

        public void setMap(HashMap<Integer, Contact> contactHashMap) {
            this.map = contactHashMap;
        }
    }
}