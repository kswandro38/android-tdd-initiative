package com.example.contactmanager.model.dummy;

import com.example.contactmanager.model.Contact;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContacts {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Contact> ITEMS = new ArrayList<Contact>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<Integer, Contact> ITEM_MAP = new HashMap<Integer, Contact>();

    private static final int COUNT = 25;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(Contact contact) {
        ITEMS.add(contact);
        ITEM_MAP.put(contact.getId(), contact);
    }

    private static Contact createDummyItem(int position) {
        return new Contact(position, "User " + position, generateRandomNumber());
    }

    private static String generateRandomNumber() {
        StringBuilder builder = new StringBuilder();
        builder.append("077");
        builder.append(new Random().nextInt(9999999-1000000)+1000000); // 1,000,000 - 9,999,999
        return builder.toString();
    }
}